# Meta Analysis Oncology

## Execution Steps:
0. Activate your virtual environment.
```bash
python -m venv venv
source ./venv/bin/activate  # Linux
source ./venv/Scripts/activate.bat #Windows
```
1. Install all the dependency packages
```bash
pip install -r requirements.txt
```

## TODO

1. Automation of research article download (PDF Format)
- pubMed article data - Ghanesh
- publicly available articles download - jayasri
- keywords:phase 2 single arm

2. Meta Analysis
- PyPdf, PyMupdf
- 